// theme file

export const lightTheme = {
    color_primary: 'black',
    color_secondary: 'blue',
    backgroundColor_primary: 'white',
    backgroundColor_secondary: 'grey'
}

export const darkTheme = {
    color_primary: 'white',
    color_secondary: 'blue',
    backgroundColor_primary: 'black',
    backgroundColor_secondary: 'grey'
}

export default {
    primary: 'gray',
    accent: 'blue',
    dark_background: 'black',
    dark_color: 'white'
}
