const screens = {
    LOGINSCREEN : 1,
    SIGNUPSCREEN : 2,
    HOMESCREEN : 3,
    HELPSCREEN : 4,
    ACCOUNTSCREEN : 5,
    ABOUTSCREEN : 6,
    SETTINGSCREEN : 7
}

export const screenName = (screenNumber) => {
    switch(screenNumber) {
      case 1:
        return 'Login'
      case 2:
        return 'Signup'
      case 3:
        // return 'Home'
        return 'Tasky'
      case 4:
        return 'Help'
      case 5:
        return 'Account'
      case 6:
        return 'About'
      case 7:
        return 'Settings'
      default:
        return 'unknown screen'
    }
}

export default screens
