// reducer function for User data

// constants
import screens from '../constants/screens'

// styles
import { lightTheme } from '../styles/theme'

const defaultUserState = {
    token: undefined,
    screenToRender : screens.LOGINSCREEN,
    name : undefined,
    email : undefined,
    theme: false // true = dark theme, false = light theme
}

export default (state = defaultUserState, action) => {
    switch(action.type) {
        case 'LOGIN': {
            return {
                ...state,
                token: action.token
            }
        }
        case 'LOGOUT': {
            return defaultUserState
            // return {
            //     ...state,
            //     token: undefined
            // }
        }
        case 'SWITCH_THEME': {
            return {
                ...state,
                theme: !state.theme
            }
        }
        case 'CHANGE_SCREEN_TO': {
            return {
                ...state,
                screenToRender: action.screenToRender
            }
        }
        case 'SET_USER_DATA': {
            return {
                ...state,
                name: action.name,
                email: action.email
            }
        }
        case 'SET_USER_STATE': {
            return {
                ...state,
                ...action.userState
            }
        }
        default: {
            return state
        }
    }
}
