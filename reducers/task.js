// reducer function for Tasks data

export default (state = [], action) => {
    switch(action.type) {
        case 'ADD_TASK': {
            return [
                ...state,
                action.task
            ]
        }
        case 'DELETE_TASK': {
            return state.filter( ({ _id }) => _id !== action._id )
        }
        case 'UPDATE_TASK': {
            return state.map((task) => {
                if(task._id === action._id) {
                    return {
                        ...task,
                        ...action.updates
                    }
                } else {
                    return task
                }
            })
        }
        case 'SET_TASKS': {
            return action.tasks
        }
        default: {
            return state
        }
    }
}
