/**
 * Redux store for Tasky-RN-app
 */

import { createStore, combineReducers, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk'

// reducers
import userReducer from '../reducers/user'
import taskReducer from '../reducers/task'

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

export default () => {
    const store = createStore( // object of key:val pair for rootProp:reducerForIt
        combineReducers({
            tasks: taskReducer,
            user: userReducer
        }),
        composeEnhancers(applyMiddleware(thunk)) // to apply redux devTools
        // window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__() // this redux devTools will not work because of Thunk
    )

    return store
}
