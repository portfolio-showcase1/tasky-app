import React, { useState, useEffect, useFonts } from 'react'
import { StyleSheet, Text, View, Modal, ActivityIndicator, StatusBar } from 'react-native'

import NetInfo from '@react-native-community/netinfo'

import AsyncStorage from '@react-native-async-storage/async-storage'

import { Provider } from 'react-redux'

// store
import configureStore from './store/configureStore'
// import { readData } from './store/localStorage'

// components
import ScreenSwitcher from './components/ScreenSwitcher'

// constants
import screens from './constants/screens'

// actions
import { setUserState, changeScreenTo } from './actions/user'
import { startSetTasks } from './actions/task'

// Redux store
const store = configureStore()

const STORAGE_KEY = '@save_user'

const readData = async (done) => {
  try {
      const userState = await AsyncStorage.getItem(STORAGE_KEY)

      if (userState !== null) {
          store.dispatch(setUserState(JSON.parse(userState)))
          store.dispatch(changeScreenTo(screens.HOMESCREEN))
          store.dispatch(startSetTasks(done))
      } else {
          done()
      }
      
  } catch (e) {
      console.log('Failed to fetch the data from storage')
      done()
  }
}

export default App = () => {
  const [modalVisible, setModalVisible] = useState(false)
  const [internetConn, setInternetConn] = useState(true)


  useEffect(() => {
    setModalVisible(true) // loading modal

    readData(() => setModalVisible(false)) // reading data from local storage

    // checking internet connection
    const unsubscribe = NetInfo.addEventListener(state => {
      // console.log('Connection type', state.type)
      // console.log('Is connected?', state.isConnected)

      setInternetConn(state.isConnected)
    })
  }, [])

  return (
    <View style={styles.screen}>

    <Modal
        animationType="fade"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => { // called when the user taps the hardware back button on Android
            // setModalVisible(false)
        }}
      >
        
        <View style={styles.loadingModal}>
          <ActivityIndicator
              size="large"
              color="#0080ff"
              animating={true}
          />
        </View>

      </Modal>

      <Modal
        animationType="fade"
        transparent={true}
        visible={!internetConn}
        onRequestClose={() => { // called when the user taps the hardware back button on Android
            // setModalVisible(false)
        }}
      >
        
        <View style={styles.connectionMessage}>
          <Text style={styles.message}>Sorry! Internet connection is required to use the app. Please connect to the internet!</Text>
        </View>

      </Modal>

      <StatusBar backgroundColor="gray" barStyle={['default','dark-content', 'light-content'][2]} />

      <Provider store={store}>
        <ScreenSwitcher />
      </Provider>
    </View>
  )
}

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    // marginTop: 20 // to display bellow status-bar
  },
  loadingModal: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(68,68,68,0.7)'
  },
  connectionMessage: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(68,68,68,0.7)',
    padding: 20
  },
  message: {
    color: 'red',
    fontWeight: 'bold',
    fontSize: 30,
    // fontFamily: 'Comici'
  }
})
