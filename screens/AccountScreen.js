import React, { useState } from 'react'
import { Text, View, Button, StyleSheet } from 'react-native'

import Icon from 'react-native-vector-icons/FontAwesome'

import AsyncStorage from '@react-native-async-storage/async-storage'

import { connect } from 'react-redux'

// actions
import { startLogout, startLogoutAll, startDeleteAccount, startUpdateUserData } from '../actions/user'

// components
import AlertBox from '../components/AlertBox'
import EditUser from '../components/EditUser'
import LinkButton from '../components/LinkButton'
import { call } from 'react-native-reanimated'

const clearStorage = async () => {
    try {
        await AsyncStorage.clear()
        console.log('Storage successfully cleared!')
    } catch (e) {
        console.log('Failed to clear the async storage.')
    }
}

const AccountScreen = (props) => {
    const [detailsModalVisible, setDetailsModalVisible] = useState(false)
    const [passwordModalVisible, setPasswordModalVisible] = useState(false)
    const [fields, setFields] = useState([])

    const accountDetailsHandler = () => {
        setFields([props.name, props.email])
        setDetailsModalVisible(true)
    }

    const changeAccountDetails = (data) => props.startUpdateUserData({name: data[0], email: data[1]})

    const passwordEditHandler = () => {
        setFields([])
        setModalVisible(true)
    }

    const handleLogout = () => AlertBox({alertTitle: 'You will logout!', alertMessage: 'Are you sure?', onYes: () => {
        props.startLogout()
        clearStorage()
    }})
        
    const handleLogoutAll = () => AlertBox({alertTitle: 'You will logout from all devices!', alertMessage: 'Are you sure?', onYes: () => {
        props.startLogoutAll()
        clearStorage()
    }})
        
    const handleDeleteAccount = () => AlertBox({alertTitle: 'Your account will be deleted!', alertMessage: 'Are you sure?', onYes: () => {
        props.startDeleteAccount()
        clearStorage()
    }})

    return (
        <View style={styles.screen}>
            {detailsModalVisible && (
                <EditUser
                    modalVisible={detailsModalVisible}
                    setModalVisible={setDetailsModalVisible}
                    fields={fields}
                    placeholders={['Neme', 'Email']}
                    onYes={changeAccountDetails}
                />
            )}

            <Icon name="user" size={20}><Text>Neme : {props.name}</Text></Icon>
            <Icon name="envelope" size={20}><Text>Email : {props.email}</Text></Icon>

            <Button title="Change your Account details" onPress={accountDetailsHandler} />
            
            <Button title="Change your password" onPress={passwordEditHandler} />
            <View style={styles.buttonsView}>
                <View style={styles.buttonItem}>
                <Icon.Button
                    name="user-times"
                    backgroundColor="#0080ff"
                    onPress={handleDeleteAccount}
                >
                Delete your account
                </Icon.Button>
                </View>

                <View style={styles.buttonItem}>
                <Icon.Button
                    name="sign-out"
                    backgroundColor="#0080ff"
                    onPress={handleLogout}
                >
                Logout
                </Icon.Button>
                </View>
                <LinkButton
                    onPress={handleLogoutAll}
                    text="Logout from all devices"
                />
            </View>
        </View>
    )
}


const styles = StyleSheet.create({
    screen: {
        flex: 1,
        // justifyContent: 'flex-start',
        alignItems: 'flex-start',
        marginTop: 20
    },
    buttonsView: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'flex-end',
        width: '100%',
        marginVertical: 30
    },
    buttonItem: {
        margin: 10
    }
})

const mapStateToProps = (state) => {
    return {
        name: state.user.name,
        email: state.user.email
    }
}

const mapDispatchToProps = (dispatch) => ({
    startLogout: () => dispatch(startLogout()),
    startLogoutAll: () => dispatch(startLogoutAll()),
    startDeleteAccount: () => dispatch(startDeleteAccount()),
    startUpdateUserData: (reqBody) => dispatch(startUpdateUserData(reqBody))
})

export default connect(mapStateToProps, mapDispatchToProps)(AccountScreen)
