// signup component

import React, { useState } from 'react'
import { ActivityIndicator, View, Text, StyleSheet, Button, Linking } from 'react-native'

import AsyncStorage from '@react-native-async-storage/async-storage'

import Icon from 'react-native-vector-icons/FontAwesome'

import { connect } from 'react-redux'

// components
import Input from '../components/Input'
import Logo from '../components/Logo'
import LinkButton from '../components/LinkButton'

// constants
import screens from '../constants/screens'

// actions
import { changeScreenTo, startSignup } from '../actions/user'

const STORAGE_KEY = '@save_user'

const saveData = async (userState) => {
    try {
      await AsyncStorage.setItem(STORAGE_KEY, JSON.stringify(userState))
    } catch (e) {
      console.log('Failed to save the data to the storage. userState==--', userState, e)
    }
}

const SignupScreen = (props) => {
    const [name, setName] = useState()
    const [email, setEmail] = useState()
    const [password, setPassword] = useState()
    const [error, setError] = useState()

    const [animating, setAnimating] = useState(false)

    const signupHandler = () => {
        if(!name || !email || !password) {
            setError('Name, Email or Password is not entered!')
            return 1
        }

        const reqBody = { name, email, password }

        props.startSignup(reqBody, saveData)
        setAnimating(true)
        // props.changeScreenTo(screens.HOMESCREEN)
    }

    const nameInputHandler = (enteredName) => {
        setName(enteredName)
    }

    const emailInputHandler = (enteredEmail) => {
        setEmail(enteredEmail)
    }

    const passwordInputHandler = (enteredPassword) => {
        setPassword(enteredPassword)
    }

    return (
        <View style={styles.screen}>

            <Logo />

            <Text>Create new account</Text>
            <View style={styles.inputContainer}>
                <Input
                    placeholder="Name"
                    onChangeText={nameInputHandler}
                    style={styles.input}
                />
                <Input
                    placeholder="Email"
                    onChangeText={emailInputHandler}
                    style={styles.input}
                />
                <Input
                    placeholder="Password"
                    onChangeText={passwordInputHandler}
                    secureTextEntry={true}
                    style={styles.input}
                />
            </View>

            <View>
                <Text style={styles.error}>{error}</Text>
            </View>

            {
                animating ? (
                    <ActivityIndicator
                        size="large"
                        color="#0080ff"
                        animating={animating}
                    />
                ) : (
                    // <Button title="Signup" onPress={signupHandler} />
                    <Icon.Button
                        name="sign-in"
                        backgroundColor="#0080ff"
                        onPress={signupHandler}
                    >
                    Signup
                    </Icon.Button>
                )
            }

                <LinkButton
                    onPress={() => props.changeScreenTo(screens.LOGINSCREEN)}
                    text="Login to existing account"
                />

            <View style={styles.footer}>
                <Text
                    style={styles.link}
                    onPress={() => Linking.openURL('https://tasky-zk.herokuapp.com/')}>
                    <Icon name="chrome" size={15}> Use webApp</Icon>
                </Text>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 20
    },
    inputContainer: {
        width: '100%',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    input: {
        width: '80%'
    },
    link: {
        color: 'blue',
        textDecorationLine: 'underline',
        padding: 20
    },
    footer: {
        position: 'absolute',
        bottom: 20,
    },
    error: {
        color:'red' 
    }
})

const mapDispatchToProps = (dispatch) => ({
    startSignup: (reqBody, callback) => dispatch(startSignup(reqBody, callback)),
    changeScreenTo: (screenToRender) => dispatch(changeScreenTo(screenToRender))
})

export default connect(undefined, mapDispatchToProps)(SignupScreen)
