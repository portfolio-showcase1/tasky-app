/*******************************************************************************************************************************
 * *****************************************************************************************************************************
 * *****************************************************************************************************************************
 * 
 *          TEST SCREEN
 * 
 * *****************************************************************************************************************************
 * *****************************************************************************************************************************
 * *****************************************************************************************************************************
 */



import React from 'react'
import { Text, View, Button, FlatList } from 'react-native'

import { connect } from 'react-redux'

import {
    Menu,
    MenuOptions,
    MenuOption,
    MenuTrigger,
  } from 'react-native-popup-menu';

// actions
import { startLogout } from '../actions/user'
import { startSetTasks } from '../actions/task'

// components
import AddTask from '../components/AddTask'

const Item = ({ description }) => (
    <View>
      <Text>{description}</Text>
    </View>
  )

const HelpScreen = (props) => {
    const renderItem = ({ item }) => (
        <Item description={item.description} />
    )

    return (
        <View>
            <Text>Help text here...</Text>
            <Text>Name : {props.name}</Text>
            <Text>Email : {props.email}</Text>
            <Button title="Get Tasks" onPress={() => props.startSetTasks()} />

            {props.tasks.length === 0 ? (
                <Text>--- No tasks ---</Text>
            ) : (
                <View>
                <Text>--- Tasks count : {props.tasks.length} ---</Text>

                <FlatList
                    data={props.tasks}
                    renderItem={renderItem}
                    keyExtractor={(item, index) => index} //item._id}
                />

                </View>
            )}
            
            <Button title="Logout" onPress={() => props.startLogout()} />
            
            <AddTask />

            {/*
            <Text>---- menu bellow ---</Text>
            <Menu>
                <MenuTrigger text=' ⋮ ' />
                <MenuOptions>
                    <MenuOption onSelect={() => console.log(`Save`)} text='Save' />
                    <MenuOption onSelect={() => console.log(`Delete`)} >
                    <Text style={{color: 'red'}}>Delete</Text>
                    </MenuOption>
                    <MenuOption onSelect={() => console.log(`Not called`)} disabled={true} text='Disabled' />
                </MenuOptions>
            </Menu>
            */}

        </View>
    )
}

/* */
const mapStateToProps = (state) => {
    return {
        name: state.user.name,
        email: state.user.email,
        tasks: state.tasks
    }
}

const mapDispatchToProps = (dispatch) => ({
    startLogout: () => dispatch(startLogout()),
    startSetTasks: () => dispatch(startSetTasks())
})

export default connect(mapStateToProps, mapDispatchToProps)(HelpScreen)
/* */

// export default HelpScreen
