import React from 'react'
import { Text, View } from 'react-native'

import { connect } from 'react-redux'

const HelpScreen = (props) => (
    <View style={{ flex: 1, backgroundColor: props.isDarkTheme ? 'black' : 'white'}}>
        <Text style={{ color: props.isDarkTheme ? 'white' : 'black'}}>Help text here...</Text>
    </View>
)

const mapStateToProps = (state) => {
    return {
        isDarkTheme: state.user.theme
    }
}

export default connect(mapStateToProps)(HelpScreen)
