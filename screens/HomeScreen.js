import React, { useEffect } from 'react'
import { View, StyleSheet, Text, FlatList, Button } from 'react-native'

// components
import AddTask from '../components/AddTask'
import TaskList from '../components/TaskList'

// constants
import Colors from '../constants/colors'

const HomeScreen = () => (
    <View style={styles.screen}>
        <TaskList />
        <AddTask />
    </View>
)

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor: Colors.dark_background
    }
})

export default HomeScreen
