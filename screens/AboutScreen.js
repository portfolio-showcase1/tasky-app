import React, { useState } from 'react'
import { Text, View, StyleSheet, Modal, Button, Linking } from 'react-native'

import Icon from 'react-native-vector-icons/FontAwesome'

import { connect } from 'react-redux'

// components
import Logo from '../components/Logo'
import MyForm from '../components/FeedbackForm'
import LinkButton from '../components/LinkButton'

const AboutScreen = (props) => {
    const [modalVisible, setModalVisible] = useState(false)
    return (
        <View style={{
            ...styles.screen,
            backgroundColor: props.isDarkTheme ? 'black' : 'white'
        }}>
            <Logo />

            <Text style={{
                color: props.isDarkTheme ? 'white' : 'black'
            }}>Version 1.6.2</Text>

            <Modal
                animationType="slide"
                // transparent={true}
                visible={modalVisible}
                onRequestClose={() => { // called when the user taps the hardware back button on Android
                    setModalVisible(false)
                }}
            >
                <View style={styles.screen}>
                    <MyForm email={props.email} onSuccess={() => setModalVisible(false)} />
                    <Button title="Cancle" onPress={() => setModalVisible(false)} />
                </View>
            </Modal>

            <Text
                style={styles.link}
                onPress={() => Linking.openURL('https://tasky-zk.herokuapp.com/')}>
                <Icon name="chrome" size={15}> Use webApp</Icon>
            </Text>

            <View style={styles.feedbackButton}>
                <LinkButton text="Send Feedback" onPress={() => setModalVisible(true)} />
            </View>

        </View>
    )
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    feedbackButton: {
        position: 'absolute',
        bottom: 10
    },
    link: {
        color: 'blue',
        textDecorationLine: 'underline',
        padding: 20
    }
})

const mapStateToProps = (state) => {
    return {
        email: state.user.email,
        isDarkTheme: state.user.theme
    }
}

export default connect(mapStateToProps)(AboutScreen)
