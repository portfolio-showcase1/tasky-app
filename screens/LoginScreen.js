// login component

import React, { useState } from 'react'
import { ActivityIndicator, View, Text, Button, StyleSheet, Linking } from 'react-native'

import AsyncStorage from '@react-native-async-storage/async-storage'

// import styled, { ThemeProvider } from 'styled-components/native'

import Icon from 'react-native-vector-icons/FontAwesome'

import { connect } from 'react-redux'

// components
import Input from '../components/Input'
import Logo from '../components/Logo'
import LinkButton from '../components/LinkButton'

// constants
import screens from '../constants/screens'

// store
// import { saveData } from '../store/localStorage'

// styles
import { lightTheme, darkTheme } from '../styles/theme'

// actions
import { changeScreenTo, startLogin, switchTheme } from '../actions/user'

/*
// start ------------- styled components playground ---------------

const Title = styled.Text`
  padding: 20px;
  font-size: 24px;
  font-weight: 500;
  color: ${props => props.theme.PRIMARY_TEXT_COLOR};
  backgroundColor: ${props => props.myProp || 'green'};
`
const BigTitle = styled(Title)`
  font-size: 30px;
`


// end ------------- styled components playground ---------------
*/

const STORAGE_KEY = '@save_user'

const saveData = async (userState) => {
    try {
      await AsyncStorage.setItem(STORAGE_KEY, JSON.stringify(userState))
    } catch (e) {
      console.log('Failed to save the data to the storage. userState==--', userState, e)
    }
}

const LoginScreen = (props) => {
    const [email, setEmail] = useState(undefined)
    const [password, setPassword] = useState(undefined)
    const [error, setError] = useState()

    const [animating, setAnimating] = useState(false)

    const loginHandler = () => {
        if(!email || !password) {
            setError('Email or Password is not entered!')
            return 1
        }

        const reqBody = { email, password }
        
        // props.startLogin(reqBody, (token) => { // arg-2: callback with TOKEN runs when login is done
        //     // storeData(token)
        // })

        props.startLogin(reqBody, saveData)
        setAnimating(true)
        props.changeScreenTo(screens.HOMESCREEN)
    }

    const emailInputHandler = (enteredEmail) => {
        setEmail(enteredEmail)
    }

    const passwordInputHandler = (enteredPassword) => {
        setPassword(enteredPassword)
    }

    return (
        <View style={styles.screen}>

            <Logo />
            
            <Text>Login to your account</Text>



        {/*
            <ThemeProvider theme={props.theme}>
                <Title myProp="yellow">Styled component</Title>
                <BigTitle>Styled component</BigTitle>
                <Button onPress={() => props.switchTheme(darkTheme)} title="dark theme" />
                <Button onPress={() => props.switchTheme(lightTheme)} title="light theme" />
            </ThemeProvider>
        */}



            <View style={styles.inputContainer}>
                <Input
                    placeholder="Email"
                    onChangeText={emailInputHandler}
                    style={styles.input}
                />
                <Input
                    placeholder="Password"
                    onChangeText={passwordInputHandler}
                    secureTextEntry={true}
                    style={styles.input}
                />
            </View>
            <View>
                <Text style={styles.error}>{error}</Text>
            </View>
          
            {
                animating ? (
                    <ActivityIndicator
                        size="large"
                        color="#0080ff"
                        animating={animating}
                    />
                ) : (
                    // <Button title="Login" onPress={loginHandler} />
                    <Icon.Button
                        name="sign-in"
                        backgroundColor="#0080ff"
                        onPress={loginHandler}
                    >
                    Login
                    </Icon.Button>
                )
            }
                
                <LinkButton
                    onPress={() => props.changeScreenTo(screens.SIGNUPSCREEN)}
                    text="Create account"
                />

            <View style={styles.footer}>
                <Text
                    style={styles.link}
                    onPress={() => Linking.openURL('https://tasky-zk.herokuapp.com/')}>
                    <Icon name="chrome" size={15}> Use webApp</Icon>
                </Text>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 20
    },
    inputContainer: {
        width: '100%',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    input: {
        width: '80%'
    },
    link: {
        color: 'blue',
        textDecorationLine: 'underline',
        padding: 20
    },
    footer: {
        position: 'absolute',
        bottom: 20
    },
    error: {
        color:'red' 
    }
})

const mapStateToProps = (state) => ({
    theme: state.user.theme
})

const mapDispatchToProps = (dispatch) => ({
    startLogin: (reqBody, callback) => dispatch(startLogin(reqBody, callback)),
    changeScreenTo: (screenToRender) => dispatch(changeScreenTo(screenToRender)),
    switchTheme: (newTheme) => dispatch(switchTheme(newTheme))
})

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen)
