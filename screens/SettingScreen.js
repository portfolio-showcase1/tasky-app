import React, { useState } from 'react'
import { Text, View, Switch, StyleSheet } from 'react-native'

import { connect } from 'react-redux'

// actions
import { switchTheme } from '../actions/user'

const SettingScreen = (props) => {
    const toggleSwitch = () => {
        props.switchTheme(!props.isDarkTheme)
    }

    return (
        <View style={[styles.screen, { backgroundColor: props.isDarkTheme ? 'black' : 'white' }]}>
            <View style={styles.switch}>
                <Text style={{ color: props.isDarkTheme ? 'white' : 'black' }}>Switch to Dark Theme : </Text>
                <Switch
                    trackColor={{ false: "#767577", true: "#81b0ff" }}
                    thumbColor={props.isDarkTheme ? "#f5dd4b" : "#f4f3f4"}
                    ios_backgroundColor="#3e3e3e"
                    onValueChange={toggleSwitch}
                    value={props.isDarkTheme}
                />
            </View>

            <View style={styles.switch}>
                <Text style={{ color: props.isDarkTheme ? 'white' : 'black' }}> Switch Sound Effects : </Text>
                <Switch
                    trackColor={{ false: "#767577", true: "#81b0ff" }}
                    thumbColor={props.isDarkTheme ? "#f5dd4b" : "#f4f3f4"}
                    ios_backgroundColor="#3e3e3e"
                    onValueChange={() => {}}
                    value={0}
                    disabled
                />
            </View>

        </View>
    )
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
        // backgroundColor: ''
    },
    switch: {
        flexDirection: "row"
    }
})

const mapStateToProps = (state) => {
    return {
        isDarkTheme: state.user.theme
    }
}

const mapDispatchToProps = (dispatch) => ({
    switchTheme: () => dispatch(switchTheme())
})

export default connect(mapStateToProps, mapDispatchToProps)(SettingScreen)
