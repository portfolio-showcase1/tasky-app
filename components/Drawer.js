import React from 'react'
import { View, Text, DrawerLayoutAndroid } from 'react-native'

const DrawerView = (props) => (
    <View style={{
        flex: 1,
        paddingTop: 50,
        backgroundColor: "#fff",
        padding: 8
    }}>
        <Text style={{ margin: 10, fontSize: 15 }}>Drawer things</Text>
        <Text
            onPress={() => props.changeScreenHandler(1)}>
            Home
        </Text>
        <Text
            onPress={() => props.changeScreenHandler(2)}>
            Help
        </Text>
    </View>
)

const Drawer = (props) => {
    return (
        <DrawerLayoutAndroid
            drawerWidth={300}
            drawerPosition="left"
            renderNavigationView={() => DrawerView(props)}
        >
        {props.children}
        </DrawerLayoutAndroid>
    )
}

export default Drawer
