// EditTask modal component

import React, { useState } from "react"
import {
  Modal,
  StyleSheet,
  Text,
  View,
  Button
} from "react-native"

import CheckBox from '@react-native-community/checkbox'

import { connect } from 'react-redux'

// components
import Input from './Input'

// actions
import { startUpdateTasks } from '../actions/task'

const EditTask = (props) => {
    const [ description, setDescription ] = useState(props.editTask.description)
    const [ completed, setCompleted ] = useState(props.editTask.completed)

    const descriptionInputHandler = (enteredDescription) => {
        setDescription(enteredDescription)
    }

    const taskEditHandler = () => {
        let updaets = { description, completed }
        let _id = props.editTask._id
        props.startUpdateTasks(_id, updaets)
        props.setModalVisible(false)
    }

  return (
    <View style={styles.centeredView}>
      <Modal
        animationType="slide"
        transparent={true}
        visible={props.modalVisible}
        onRequestClose={() => { // called when the user taps the hardware back button on Android
            props.setModalVisible(false)
        }}
      >
        
        <View style={styles.screen}>
            <View style={styles.inputView}>
              <Text>Description : </Text>
              <Input
                  placeholder="Enter description"
                  onChangeText={descriptionInputHandler}
                  style={styles.inputStyle}
                  value={description}
                  multiline={true}
              />
            </View>

            <View style={styles.inputView}>
                <Text>Completed</Text>
                <CheckBox
                    disabled={false}
                    value={completed}
                    onValueChange={(newValue) => setCompleted(newValue)}
                />
            </View>

            <View style={styles.buttonView}>
              <Button title="Cancle" onPress={() => props.setModalVisible(false)} />
              <Button title="Save" onPress={taskEditHandler} />
            </View>
        </View>

      </Modal>
    </View>
  )
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        // margin: 50
    },
    inputView: {
      flexDirection: 'row',
      justifyContent: 'flex-start',
      alignItems: 'center'
    },
    buttonView: {
      margin: 20,
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
    }
})

const mapDispatchToProps = (dispatch) => ({
    startUpdateTasks: (_id, updaets) => dispatch(startUpdateTasks(_id, updaets))
})

export default connect(undefined, mapDispatchToProps)(EditTask)
