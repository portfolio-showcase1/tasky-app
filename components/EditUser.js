// edit user modal component, speciled designed for two fiend input

import React, { useState } from "react"
import {
  Modal,
  StyleSheet,
  Text,
  View,
  Button
} from "react-native"

// components
import Input from './Input'

const EditUser = (props) => {
    const [ field1, setField1 ] = useState(props.fields[0])
    const [ field2, setField2 ] = useState(props.fields[1])

    const field1Handler = (newData) => {
        setField1(newData)
    }

    const field2Handler = (newData) => {
        setField2(newData)
    }

    const editHandler = () => {
        props.onYes([field1, field2])
        props.setModalVisible(false)
    }

  return (
    <View style={styles.centeredView}>
      <Modal
        animationType="slide"
        transparent={true}
        visible={props.modalVisible}
        onRequestClose={() => { // called when the user taps the hardware back button on Android
            props.setModalVisible(false)
        }}
      >
        
        <View style={styles.screen}>
            <Input
                placeholder={props.placeholders[0]}
                onChangeText={field1Handler}
                style={styles.inputStyle}
                value={field1}
            />
            <Input
                placeholder={props.placeholders[1]}
                onChangeText={field2Handler}
                style={styles.inputStyle}
                value={field2}
            />

            <Button title="Save" onPress={editHandler} />
            <Button title="Cancle" onPress={() => props.setModalVisible(false)} />
        </View>

      </Modal>
    </View>
  )
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        // margin: 50
    }
})

export default EditUser
