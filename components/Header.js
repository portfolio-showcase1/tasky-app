import React, { useEffect } from 'react'
import { View, Text, StyleSheet, BackHandler } from 'react-native'

import { connect } from 'react-redux'


import {
    Menu,
    MenuOptions,
    MenuOption,
    MenuTrigger,
  } from 'react-native-popup-menu'

import Icon from 'react-native-vector-icons/FontAwesome'

// actions
import { changeScreenTo } from '../actions/user'

// constants
import Colors from '../constants/colors'
import screens, { screenName } from '../constants/screens'

const Header = (props) => {

    // to go back using hardware button
      useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', function () {
            if (props.screenToRender !== screens.HOMESCREEN) {
                props.changeScreenTo(screens.HOMESCREEN)

                return true
            }
                return false
          })
      }, [props.screenToRender])

    return (
        <View style={styles.header}>

            {
                props.screenToRender !== screens.HOMESCREEN && <Text style={styles.menuIcon} onPress={() => props.changeScreenTo(screens.HOMESCREEN)}>
                        <Icon name={'arrow-left'} size={30} color='white' />
                    </Text>
            }

            <Text style={styles.headerTitle}>{screenName(props.screenToRender)}</Text>

            {
                props.screenToRender === screens.HOMESCREEN && (
                    <Menu>
                        <MenuTrigger>
                            <Text style={styles.menuIcon}><Icon name={'ellipsis-v'} size={30} color='white' /></Text>
                        </MenuTrigger>
                        <MenuOptions>
                            <MenuOption onSelect={() => props.changeScreenTo(screens.ACCOUNTSCREEN)} >
                                <Text style={{color: 'red'}}>
                                    <Icon name={'user-circle'} size={styles.menuOptionsIcon.fontSize} color={styles.menuOptionsIcon.color}>
                                        Account
                                    </Icon>
                                </Text>
                            </MenuOption>

                            <MenuOption onSelect={() => props.changeScreenTo(screens.SETTINGSCREEN)} >
                                <Text style={{color: 'red'}}>
                                    <Icon name={'cog'} size={styles.menuOptionsIcon.fontSize} color={styles.menuOptionsIcon.color}>
                                        Settings
                                    </Icon>
                                </Text>
                            </MenuOption>

                            <MenuOption onSelect={() => props.changeScreenTo(screens.HELPSCREEN)} >
                                <Text style={{color: 'red'}}>
                                    <Icon name={'question-circle'} size={styles.menuOptionsIcon.fontSize} color={styles.menuOptionsIcon.color}>
                                        Help
                                    </Icon>
                                </Text>
                            </MenuOption>

                            <MenuOption onSelect={() => props.changeScreenTo(screens.ABOUTSCREEN)} >
                                <Text style={{color: 'red'}}>
                                    <Icon name={'info-circle'} size={styles.menuOptionsIcon.fontSize} color={styles.menuOptionsIcon.color}>
                                        About
                                    </Icon>
                                </Text>
                            </MenuOption>


                            {/* ======= for reference ======
                                <MenuOption onSelect={() => props.changeScreenTo(screens.HELPSCREEN)} text='Help' />
                                <MenuOption onSelect={() => console.log(`Delete`)} >
                                    <Text style={{color: 'red'}}>Delete</Text>
                                </MenuOption>
                                <MenuOption onSelect={() => console.log(`Not called`)} disabled={true} text='Disabled' />
                            */}
                        </MenuOptions>
                    </Menu>
                )
            }

        </View>
    )
}

const styles = StyleSheet.create({
    header: {
        // flex: 1,
        alignItems: 'center',
        justifyContent: 'space-between',
        flexDirection: 'row',
        backgroundColor: Colors.primary,
        width: '100%',
        padding: 8,
        // paddingTop: 15,

        // position: 'absolute',
        // top: 10
        // zIndex: 2
    },
    headerTitle: {
        color: Colors.dark_color,
        fontSize: 25
    },
    menuIcon: {
        // backgroundColor: 'pink',
        // borderWidth
        // borderColor
        // borderRadius
        padding: 10,
        paddingHorizontal: 20
        // margin: 50,
        // fontSize
    },
    menuOptionsIcon: {
        fontSize: 20,
        color: 'black'
    }
})

const mapStateToProps = (state) => {
    return {
        screenToRender: state.user.screenToRender
    }
}

const mapDispatchToProps = (dispatch) => ({
    changeScreenTo: (screenNumber) => dispatch(changeScreenTo(screenNumber))
})

export default connect(mapStateToProps, mapDispatchToProps)(Header)
