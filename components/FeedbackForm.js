// Customize this 'myform.js' script and add it to your JS bundle.
// Then import it with 'import MyForm from "./myform.js"'.
// Finally, add a <MyForm/> element whereever you wish to display the form.

import React, { useState } from "react"
import { Text, View, StyleSheet, TextInput, Button, ToastAndroid } from 'react-native'

import axios from 'axios'

const MyForm = (props) => {
    const [status, setState] = useState()
    // const [email, setEmail] = useState()
    const email = props.email
    const [message, setMessage] = useState()

    const submitHandler = () => {
            let reqBody = { email, message }
            let feedbackFormURL = 'https://formspree.io/f/moqpkezj'

            axios({
                method: 'post',
                url: feedbackFormURL,
                data: reqBody
            }).then((response) => {
                // console.log('feedback form response... : ', response)
                props.onSuccess()
                response.data.ok ? ToastAndroid.show("Sent. Thanks for feedback.", ToastAndroid.SHORT) : ToastAndroid.show("Something went wronge! Try again.", ToastAndroid.SHORT)
            }).catch((error) => {
                // setError(error.message)
            })
    }

    return (
      <View style={styles.container}>
        <TextInput
            placeholder="Enter feedback message"
            onChangeText={(val) => setMessage(val)}
            value={message}
            multiline={true}
            style={styles.textInput}
        />
        
        <Button title="Send" onPress={submitHandler} />
      </View>
    )
}

const styles = StyleSheet.create({
    container: {
        // flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    textInput: {
        backgroundColor: 'grey',
        color: 'white',
        height: 100,
        padding: 10,
        margin: 10
    }
})

export default MyForm
