import React, { useState, useEffect } from 'react'
import { Text, View, StyleSheet, TouchableOpacity, FlatList, RefreshControl, TextInput } from 'react-native'

import Icon from 'react-native-vector-icons/FontAwesome'

import { connect } from 'react-redux'

// components
import TaskItem from './TaskItem'
import EditTask from './EditTask'

// actions
import { startSetTasks } from '../actions/task'

const TaskList = (props) => {
    const [modalVisible, setModalVisible] = useState(false)
    const [editTask, setEditTask] = useState(undefined)
    const [refreshing, setRefreshing] = useState(false)
    const [sortUncompleted, setSortUncompleted] = useState(false)
    const [search, setSearch] = useState()
    const [searched, setSearched] = useState(false)
    const [tasks, setTasks] = useState(props.tasks)
    const [adVisible, setAdVisible] = useState(true)


    // let tasks = [...props.tasks] // clone, saving original order in `props.tasks`
    // let tasks = props.tasks.slice() // clone, saving original order in `props.tasks`
    // let tasks = props.tasks

    const handleSort = () => {
        if(sortUncompleted) {
            setSortUncompleted(false)
            setTasks(props.tasks)
            console.log('un-sorting...', sortUncompleted)
        } else {
            setTasks(tasks.sort((a, b) => {
                if(!a.completed && b.completed) {
                    return -1 // a less
                } else if(a.completed && !b.completed) {
                    return 0 // same
                } else {
                    return 1 // b less
                }
        
            }))
            setSortUncompleted(true)
            console.log('sorting...', sortUncompleted)
        }
        
    }

    const closeAd = () => {
        setAdVisible(false)
    }

    const handleSearch = () => {
        
        if(search && !searched) {
            setTasks(tasks.filter((task) => {
                if(task.description.includes(search)) {
                    return true
                }

                return false
            }))
            setSearched(true)
        } else if(search && searched) {
            setSearch('')
            setTasks(props.tasks)
            setSearched(false)
        } else {
            setTasks(props.tasks)
            setSearched(false)
        }
    }

    const tasksDetails = () => {
        let remaining = tasks.filter((task) => task.completed == false).length

        return [remaining, tasks.length - remaining, tasks.length] // remaining, completed, total
    }

    useEffect(() => {
        setTasks(props.tasks)
        handleSort()
        handleSearch()
        tasksDetails()
    }, [props.tasks])

    const onRefresh = React.useCallback(() => {
        setRefreshing(true);
    
        props.startSetTasks(() => setRefreshing(false)) // 1-arg: runs when req. is completed
      }, [])

    handleEditTask = (task) => {
        setEditTask(task)
        setModalVisible(true)
    }

    return (
        <View style={styles.screen}>
            {modalVisible && (
                <EditTask
                    modalVisible={modalVisible}
                    setModalVisible={setModalVisible}
                    editTask={editTask}
                />
            )}

            {props.tasks.length === 0 ? (
                <Text style={styles.infoText}>No tasks</Text>
            ) : (
                <View>

                <View style={styles.info}>
                    {/* <Text style={styles.infoText}>Tasks count : {props.tasks.length}</Text> */}
                    <Text style={styles.infoText}>
                        {tasksDetails()[0]} <Text style={{backgroundColor: "red", fontSize: 10}}>remaining</Text>
                        + {tasksDetails()[1]} <Text style={{backgroundColor: "green", fontSize: 10}}>completed</Text>
                        = {tasksDetails()[2]} <Text style={{backgroundColor: "white", color: "black", fontSize: 10}}>total</Text>
                    </Text>
                    <TextInput
                        placeholder="Search tasks"
                        onChangeText={setSearch}
                        value={search}
                        style={{color: 'white'}}
                    />
                    <Text onPress={handleSearch}><Icon name={searched ? 'times' : 'search'} size={25} color='white' /></Text>
                    <Text onPress={handleSort}><Icon name={'sort'} size={30} color={sortUncompleted ? 'lightgreen' : 'white'} /></Text>
                </View>

                {
                    adVisible && <TouchableOpacity style={styles.adView} onPress={() => console.log("ad...Ad...Ad")}>
                        <Text style={{color: "lightgreen"}}>Hire the developer!</Text>
                        <Icon name={'times-circle'} size={30} color="red" onPress={closeAd} />
                    </TouchableOpacity>
                }
                <FlatList
                    data={tasks} //TODO: bugs here
                    // data={props.tasks}

                    renderItem={ ({ item }) => <TaskItem task={item} handleEditTask={handleEditTask} /> }
                    keyExtractor={(item, index) => item._id}
                    refreshControl={
                        <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
                    }
                />

                </View>
            )}
        </View>
    )
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'black',
        width: '100%'
    },
    adView: {
        flexDirection: "row",
        justifyContent: "space-between",
        marginHorizontal: 20,
        marginVertical: 10,
        backgroundColor: "orange",
        padding: 10
    },
    info: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: 'black',
        padding: 5,
        paddingHorizontal: 20,
        width: '100%'
    },
    infoText: {
        fontSize: 20,
        color: 'white'
    }
})

const mapStateToProps = (state) => {
    return {
        tasks: state.tasks
    }
}

const mapDispatchToProps = (dispatch) => ({
    startSetTasks: (callback) => dispatch(startSetTasks(callback))
})

export default connect(mapStateToProps, mapDispatchToProps)(TaskList)
