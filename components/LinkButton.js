import React from 'react'
import { Text, View, StyleSheet } from 'react-native'

const LinkButton = (props) => (
    <View>
        <Text
            style={styles.link}
            onPress={props.onPress}>
            {props.text}
        </Text>
    </View>
)

const styles = StyleSheet.create({
    link: {
        // color: 'blue',
        textAlign: 'center',
        textAlignVertical: 'center',
        borderColor: '#0080ff',
        borderWidth: 3,
        borderRadius: 5,
        padding: 3,
        margin: 10
    },
})

export default LinkButton
