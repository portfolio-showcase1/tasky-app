import React from "react";
import { StyleSheet, Alert } from "react-native";

const AlertBox = (props) => Alert.alert(
    props.alertTitle,
    props.alertMessage,
    [
      {
        text: "Cancel",
        // onPress: () => console.log("Cancel Pressed"),
        onPress: () => {},
        style: "cancel"
      },
      { text: "OK", onPress: props.onYes }
    ],
    { cancelable: false }
  )

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "space-around",
    alignItems: "center"
  }
});

export default AlertBox