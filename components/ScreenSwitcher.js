import React from 'react';
import { StyleSheet, View } from 'react-native'

import { MenuProvider } from 'react-native-popup-menu'

import { connect } from 'react-redux'

// constants
import screens from '../constants/screens'

// components
import Header from './Header'

// screens
import AboutScreen from '../screens/AboutScreen'
import AccountScreen from '../screens/AccountScreen'
import HelpScreen from '../screens/HelpScreen'
import HomeScreen from '../screens/HomeScreen'
import LoginScreen from '../screens/LoginScreen'
import SignupScreen from '../screens/SignupScreen'
import SettingScreen from '../screens/SettingScreen'

let screenContent

const ScreenSwitcher = (props) => {

  if(props.token) {
    switch (props.screenToRender) {
      case screens.HELPSCREEN:
        screenContent = <HelpScreen />
        break;
      case screens.LOGINSCREEN:
        screenContent = <LoginScreen />
        break;
      case screens.HOMESCREEN:
        screenContent = <HomeScreen />
        break;
      case screens.ABOUTSCREEN:
        screenContent = <AboutScreen />
        break;
      case screens.ACCOUNTSCREEN:
        screenContent = <AccountScreen />
        break;
      case screens.SETTINGSCREEN:
        screenContent = <SettingScreen />
        break;
      default:
        screenContent = <HelpScreen />
        break;
    }
  } else {
    switch (props.screenToRender) {
      case screens.SIGNUPSCREEN:
        screenContent = <SignupScreen />
        break;
      default:
        screenContent = <LoginScreen />
        break;
    }
  }

  return (
    <View style={styles.screen}>
    <MenuProvider>
        {
          props.token && <View style={styles.header}><Header /></View>
        }
        <View style={styles.content}>{screenContent}</View>
        </MenuProvider>
    </View>
  )
}

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    // backgroundColor: 'red',
    flexDirection: 'column',
    // justifyContent: 'center',
    // alignItems: 'center'
  },
  content: {
    flex: 1,
    // backgroundColor: 'green'
  },
  header: {
    flex: 1/10,
    // marginBottom: 10,
    // paddingBottom: 10,
    // backgroundColor: 'blue'
  }
});

const mapStateToProps = (state) => {
    return {
        screenToRender: state.user.screenToRender,
        token: state.user.token
    }
}

export default connect(mapStateToProps)(ScreenSwitcher)
