import React from 'react'
import { View, StyleSheet, Text, ToastAndroid, Dimensions } from 'react-native'

import CheckBox from '@react-native-community/checkbox'

import { Audio } from 'expo-av'

import Icon from 'react-native-vector-icons/FontAwesome'

import { connect } from 'react-redux'

// components
import Card from './Card'
import AlertBox from './AlertBox'

// actions
import { startDeleteTasks, startCheckChange } from '../actions/task'

const TaskItem = (props) => {
    const [sound, setSound] = React.useState();

    const handleDeleteTask = () => AlertBox({
        alertTitle: 'Task will be deleted!',
        alertMessage: 'Are you sure?',
        onYes: () => {
            props.startDeleteTasks(props.task._id)
            playSound('delete')
        }
    })
 
    const handleEditTask = () => {
        props.handleEditTask(props.task)
    }

    // loading sound
    async function playSound(soundType) {
        // console.log('Loading Sound');
        const { sound } = soundType == 'completed' ? await Audio.Sound.createAsync(
            require('../assets/audios/completed_sound.wav')
         ) : await Audio.Sound.createAsync(
            require('../assets/audios/deleting_sound.mp3')
         )
        setSound(sound);
    
        // console.log('Playing Sound');
        await sound.playAsync();
    }
    
    // unloading sound
    React.useEffect(() => {
        return sound
            ? () => {
                // console.log('Unloading Sound');
                sound.unloadAsync(); }
            : undefined;
    }, [sound]);

    const handleCheckChange = (newValue) => {
        let updates = {
            description: props.task.description,
            completed: newValue
        }

        props.startCheckChange(props.task._id, updates, async () => {
            // callback, runs on successful change on database

            let message = newValue ? 'completed.' : 'incomplete!'
            ToastAndroid.show(`Task ${message}`, ToastAndroid.SHORT)

            // playing task completion audio file
            // backgroundMusic = new Audio.Sound()
            // try {
            //   await backgroundMusic.loadAsync(
            //     require('../assets/audios/completed_sound.wav')
            //   )
            //   await backgroundMusic.setIsLoopingAsync(true)
            //   await backgroundMusic.playAsync()
            //   // Your sound is playing!
            // } catch (error) {
            //   // An error occurred!
                
            // }
            playSound('completed')

        })
        
    }

    return (
        <View style={styles.container}>
            {/* <Card style={styles.taskItem}> */}
            <Card style={{
                flex: 1,
                flexDirection: 'row',
                marginBottom: 5,
                justifyContent: 'space-between',
                alignItems: 'center',
                width: Dimensions.get('screen').width,
                backgroundColor: props.task.completed ? 'darkblue' : 'grey',
            }}>
                <CheckBox
                    disabled={false}
                    value={props.task.completed}
                    onValueChange={(newValue) => handleCheckChange(newValue)}
                />
                <Text
                    style={[props.task.completed && styles.task_completed, styles.taskDescription]}
                >
                    {props.task.description}
                </Text>
                <View style={styles.buttons}>

                    {/* === for reference ===

                    <Icon.Button
                        name="trash"
                        backgroundColor="#3b5998"
                        onPress={handleDeleteTask}
                    >
                    delete
                    </Icon.Button>
                    <View>
                        <Icon name={'trash'} size={30} color="#900" />
                    </View>
                    */}
                    <Icon.Button
                        name="edit"
                        backgroundColor="#3b5998"
                        onPress={handleEditTask}
                    >
                    </Icon.Button>
                    <Icon.Button
                        name="trash"
                        backgroundColor="#3b5998"
                        onPress={handleDeleteTask}
                    >
                    </Icon.Button>
                </View>
            </Card>
        </View>
    )
}

const styles = StyleSheet.create({
    buttons: {
        flex: 0.6,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    taskDescription: {
        flex: 1,
        flexWrap: "wrap",
        color: 'white'
    },
    taskItem: {
        flex: 1,
        flexDirection: 'row',
        marginBottom: 5,
        justifyContent: 'space-between',
        alignItems: 'center',
        width: Dimensions.get('screen').width,
        backgroundColor: 'grey',
    },
    task_completed: {
        textDecorationLine: 'line-through'
    },
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: 10,
    }
})


const mapDispatchToProps = (dispatch) => ({
    startDeleteTasks: (_id) => dispatch(startDeleteTasks(_id)),
    startCheckChange: (_id, updates, callback) => dispatch(startCheckChange(_id, updates, callback))
})

export default connect(undefined, mapDispatchToProps)(TaskItem)
