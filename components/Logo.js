import React from 'react'
import { Text, View, StyleSheet } from 'react-native'

const Logo = () => (
    <View style={styles.container}>
        <Text style={styles.title}>Tasky</Text>
        <Text style={styles.subTitle}>Get your goal</Text>
    </View>
)

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 50,
        marginTop: -100
    },
    title: {
        fontSize: 50,
        fontWeight: 'bold',
        fontFamily: "Roboto"
    },
    subTitle: {
        fontSize: 15
    }
})

export default Logo
