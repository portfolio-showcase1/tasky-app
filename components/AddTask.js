// AddTask component

import React, { useState } from 'react'
import { View, StyleSheet, Text, Button } from 'react-native'

import CheckBox from '@react-native-community/checkbox'

import Icon from 'react-native-vector-icons/FontAwesome'

import { connect } from 'react-redux'

// components
import Input from './Input'

// constants
import Colors from '../constants/colors'

// actions
import { startAddTask } from '../actions/task'

const AddTask = (props) => {
    const [ description, setDescription ] = useState('')
    const [ completed, setCompleted ] = useState(false)


    const descriptionInputHandler = (enteredDescription) => {
        setDescription(enteredDescription)
    }

    const handleAddTask = () => {
        let reqBody = {description, completed}
        
        props.startAddTask(reqBody)

        // resetting values
        setDescription('')
        setCompleted(false)
    }


    return (
        <View style={styles.screen}>
            <Input
                placeholder="Enter description"
                onChangeText={descriptionInputHandler}
                value={description}
                style={styles.inputStyle}
                multiline={true}
            />

            <View style={styles.inputView}>
                <Text style={styles.checkBox}>Completed</Text>
                <CheckBox
                    disabled={false}
                    value={completed}
                    onValueChange={(newValue) => setCompleted(newValue)}
                />
            </View>

            <Icon.Button
                name="plus"
                backgroundColor="#3b5998"
                onPress={handleAddTask}
            >
            </Icon.Button>
        </View>
    )
}

const styles = StyleSheet.create({
    screen: {
        alignItems: 'center',
        justifyContent: 'space-between',
        flexDirection: 'row',
        backgroundColor: Colors.primary,
        width: '100%',
        padding: 8,
    },
    inputStyle: {
        borderBottomColor: 'white',
        color: 'white',
        flex: 1
    },
    inputView: {
        flexDirection: "row",
        alignItems: 'center'
    },
    checkBox: {
        color: 'white'
    },
    inputContainer: {
        width: '100%',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    link: {
        color: 'blue',
        textDecorationLine: 'underline'
    },
    footer: {
        position: 'absolute',
        bottom: 20
    },
    error: {
        color:'red' 
    }
})

const mapDispatchToProps = (dispatch) => ({
    startAddTask: (reqBody) => dispatch(startAddTask(reqBody))
})

export default connect(undefined, mapDispatchToProps)(AddTask)

