// task action for Tasky-RN-app

import axios from 'axios'

// constants
import Urls from '../constants/urls'

export const addTask = (task) => ({
    type: 'ADD_TASK',
    task
})

export const deleteTask = (_id) => ({
    type: 'DELETE_TASK',
    _id
})

export const updateTask = (_id, updates) => ({
    type: 'UPDATE_TASK',
    _id,
    updates
})

export const setTasks = (tasks) => ({
    type: 'SET_TASKS',
    tasks
})

//
// Async. Actions
//

export const startAddTask = (reqBody) => {
    return (dispatch, getState) => {
        axios({
            method: 'post',
            url: Urls.API_URL + '/tasks',
            headers: { Authorization: "Bearer " + getState().user.token },
            data: reqBody
        }).then((response) => {
            // console.log('respo add task', response)
            dispatch(addTask(response.data))
        }).catch((error) => {
            // setError(error.message)
        })
    }
}

export const startSetTasks = (callback) => {
    return (dispatch, getState) => {
        axios({
            method: 'get',
            url: Urls.API_URL + '/tasks',
            headers: { Authorization: "Bearer " + getState().user.token }
        }).then((response) => {
            dispatch(setTasks(response.data))
            callback()
        }).catch((error) => {
            // setError(error.message)
        })  
    }
}

export const startDeleteTasks = (_id) => {
    return (dispatch, getState) => {

        axios({
            method: 'delete',
            url: Urls.API_URL + '/tasks/' + _id,
            headers: { Authorization: "Bearer " + getState().user.token },
        }).then((response) => {
            dispatch(deleteTask(response.data._id))
        }).catch((error) => {
            // setError(error.message)
        })
    }
}

export const startUpdateTasks = (_id, updates) => {
    return (dispatch, getState) => {

        axios({
            method: 'patch',
            url: Urls.API_URL + '/tasks/' + _id,
            headers: { Authorization: "Bearer " + getState().user.token },
            data: updates
        }).then((response) => {
            dispatch(updateTask(response.data._id, updates))
        }).catch((error) => {
            // setError(error.message)
        })
    }
}

export const startCheckChange = (_id, updates, callback) => {
    return (dispatch, getState) => {

        axios({
            method: 'patch',
            url: Urls.API_URL + '/tasks/' + _id,
            headers: { Authorization: "Bearer " + getState().user.token },
            data: updates
        }).then((response) => {
            dispatch(updateTask(response.data._id, updates))
            callback()
        }).catch((error) => {
            // setError(error.message)
        })
    }
}
