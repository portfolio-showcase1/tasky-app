// user action for Tasky-RN-app

import axios from 'axios'

// constants
import Urls from '../constants/urls'

// actions
import { startSetTasks, setTasks } from './task'

export const switchTheme = () => {
    return dispatch => {
        dispatch({
            type: 'SWITCH_THEME'
        })
    }
}

export const login = (token) => ({
    type: 'LOGIN',
    token
})

export const logout = () => ({
    type: 'LOGOUT'
})

export const changeScreenTo = (screenToRender) => ({
    type: 'CHANGE_SCREEN_TO',
    screenToRender
})

export const setUserData = (user) => ({
    type: 'SET_USER_DATA',
    name: user.name,
    email: user.email
})

export const setUserState = (userState) => ({
    type: 'SET_USER_STATE',
    userState
})

//
// Async. Actions
//

export const startLogin = (reqBody, callback) => {
    return (dispatch) => {
        axios({
            method: 'post',
            url: Urls.API_URL + '/users/login',
            data: reqBody
        }).then((response) => {
            dispatch(login(response.data.token))
            dispatch(setUserData(response.data.user))
            dispatch(startSetTasks()) // loading user's task data on login

            callback({ // it is save data function of AsyncStorage
                token: response.data.token,
                name : response.data.user.name,
                email : response.data.user.email,
            })
        }).catch((error) => {
            // setError(error.message)
        })
    }
}

export const startSignup = (reqBody, callback) => {
    return (dispatch) => {
        axios({
            method: 'post',
            url: Urls.API_URL + '/users',
            data: reqBody
        }).then((response) => {
            dispatch(login(response.data.token))
            dispatch(setUserData(response.data.user))

            callback({ // it is save data function of AsyncStorage
                token: response.data.token,
                name : response.data.user.name,
                email : response.data.user.email,
            })
        }).catch((error) => {
            // setError(error.message)
        })
    }
}

export const startLogout = () => {
    return (dispatch, getState) => {
        axios({
            method: 'post',
            url: Urls.API_URL + '/users/logout',
            headers: { Authorization: "Bearer " + getState().user.token }
        }).then((response) => {
            dispatch(logout())
            dispatch(setTasks([]))
        }).catch((error) => {
            // setError(error.message)
        })
    }
}

export const startLogoutAll = () => {
    return (dispatch, getState) => {
        axios({
            method: 'post',
            url: Urls.API_URL + '/users/logoutAll',
            headers: { Authorization: "Bearer " + getState().user.token }
        }).then((response) => {
            dispatch(logout())
            dispatch(setTasks([]))
        }).catch((error) => {
            // setError(error.message)
        })
    }
}

export const startDeleteAccount = () => {
    return (dispatch, getState) => {
        axios({
            method: 'delete',
            url: Urls.API_URL + '/users/me',
            headers: { Authorization: "Bearer " + getState().user.token }
        }).then((response) => {
            dispatch(logout())
            dispatch(setTasks([]))
        }).catch((error) => {
            // setError(error.message)
        })
    }
}

//TODO: not working API bug found
export const startUpdateUserData = (reqBody) => {
    return (dispatch, getState) => {
        console.log('onld user', reqBody)
        axios({
            method: 'patch',
            url: Urls.API_URL + '/users/me',
            headers: { Authorization: "Bearer " + getState().user.token },
            body: reqBody
        }).then((response) => {
            console.log('new user', response.data)
            dispatch(setUserData(response.data))
        }).catch((error) => {
            // setError(error.message)
        })
    }
}
