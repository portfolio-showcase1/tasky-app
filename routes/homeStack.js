import { createStackNavigator } from 'react-navigation-stack'
import { createAppContainer } from 'react-navigation'

import React from 'react'
import { View, Text, Button } from 'react-native'

const Home = (props) => {
    const pressHandler = () => {
        props.navigation.navigate('About')
    }

    return (
        <View>
            <Text>Home screen</Text>
            <Button title="go to About" onPress={pressHandler} />
        </View>
    )
}

const About = (props) => {
    return (
        <View>
            <Text>About screen</Text>
        </View>
    )
}

const screens = {
    Home: {
        screen: Home
    },
    About: {
        screen: About
    }
}

const HomeStack = createStackNavigator(screens)

export default createAppContainer(HomeStack)
